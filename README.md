# GameCube Homebrew for n00bs

Instructions and scripts for bootstrapping GameCube homebrew

See [GameCube Homebrew for n00bs - Ultimate Guide](https://coolaj86.com/articles/gamecube-homebrew-guide-for-n00bs/)

# SD Homebrew Media Creator

This script will:

1. Create a `~/Downloads/GC_HMBRW` folder with everything you need
2. Prompt for an SD device to format (and then format it)
3. Copy `~/Downloads/GC_HMBRW` to the SD card

```bash
curl https://git.coolaj86.com/coolaj86/GameCube-Homebrew-for-n00bs/raw/branch/master/create-sd-media-launcher-macos.sh -o create-sd-media-launcher-macos.sh
bash ./create-sd-media-launcher-macos.sh
```

# Homebrew Cheatsheet
Downloads:

| Homebrew                              |   Version | Homepage                                                                                                  | zip                                                                                      | tar                                                                                         | 7z                                                                                  |
| :------------------------------------ | --------: | :-------------------------------------------------------------------------------------------------------- | :--------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------- | ----------------------------------------------------------------------------------- |
| **Swiss**                             | v0.5 r927 | [github.com/emukidid/swiss-gc](https://github.com/emukidid/swiss-gc)                                      | [zip](https://github.com/solderjs/swiss-gc/releases/download/v0.5r927/swiss_r927.zip)    | [tar.xz](https://github.com/emukidid/swiss-gc/releases/download/v0.5r927/swiss_r927.tar.xz) | [7z](https://github.com/emukidid/swiss-gc/releases/download/v0.5r927/swiss_r927.7z) |
| **GBI** / Game Boy Interface          |         - | [GC Forever Wiki: Game Boy Interface](https://www.gc-forever.com/wiki/index.php?title=Game_Boy_Interface) | [zip](https://files.extremscorner.org/gamecube/apps/gbi/latest)                          | -                                                                                           | -                                                                                   |
| **GCMM** / GameCube Memory Manager    |      1.4f | [github.com/suloku/gcmm](https://github.com/suloku/gcmm)                                                  | [zip](https://github.com/suloku/gcmm/releases/download/1.4f/gcmm_1.4f.zip)               | -                                                                                           | -                                                                                   |
| **WWHack** / Wind Waker Gamesave Hack |    v1.1.1 | [github.com/FIX94/ww-hack-gc](https://github.com/FIX94/ww-hack-gc)                                        | [zip](https://github.com/FIX94/ww-hack-gc/releases/download/v1.1.1/WWHack-GC-v1.1.1.zip) | -                                                                                           | -                                                                                   |
| Other Gamesave Hacks                  |         - | [GC Homebrew.com](https://gchomebrew.com)     | -                                                                                        | -                                                                                           | -                                                                                   |

Convert 7z / rar to zip:

* https://extract.me

Special Files:

|      name      | desc |
| -------------: | :--- |
| `autoexec.dol` | run by Datel SD Media Launcher Action Replay on disc load |
| `boot.dol`     | run by Swiss on load |
| `xxxx.cli`     | text file with list of arguments Swiss should use when loading `xxxx.dol` |
| `boot.gci`     | whatever WWHack should boot |