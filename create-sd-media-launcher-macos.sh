#!/bin/bash

set -e
set -u

my_swiss_ver=r927
my_wwhack_ver=v1.1.1
my_gcmm_ver=1.4f
#my_gbi_ver=20191107
my_gbi_ver=latest
my_sd_name=GC_HMBRW

pushd ~/Downloads >/dev/null
    echo ''
    echo "Creating ~/Downloads/${my_sd_name}"
    rm -rf ./"${my_sd_name}"
    mkdir -p ./"${my_sd_name}"/MCBACKUP/

    echo ''
    echo 'Download and extract Swiss (Zip)...'
    rm -rf swiss*
    curl -fL https://github.com/coolaj86/swiss-gc/releases/download/v0.5${my_swiss_ver}/swiss_${my_swiss_ver}.zip \
        -o swiss_${my_swiss_ver}.zip
    unzip -q swiss_${my_swiss_ver}.zip
    #wget https://github.com/emukidid/swiss-gc/releases/download/v0.4r714/swiss_r714.7z
    #7z x swiss_r714.7z
    # Copy swiss as an auto-boot for SD Launcher (does nothing otherwise)
    rsync -avP swiss_${my_swiss_ver}/DOL/swiss_${my_swiss_ver}-compressed.dol ./"${my_sd_name}"/
    rsync -avP swiss_${my_swiss_ver}/DOL/swiss_${my_swiss_ver}-compressed.dol ./"${my_sd_name}"/autoexec.dol

    echo ''
    echo 'Download and extract GCMM ...'
    rm -rf gcmm*
    curl -fsSL https://github.com/suloku/gcmm/releases/download/${my_gcmm_ver}/gcmm_${my_gcmm_ver}.zip -o gcmm_${my_gcmm_ver}.zip
    unzip -q gcmm_${my_gcmm_ver}.zip
    # Copy GCMM as a bootable dol (which will be launched from Swiss)
    rsync -avP gcmm_${my_gcmm_ver}/gamecube/gcmm_*.dol ./"${my_sd_name}"/

    echo ''
    echo 'Download and extract Wind Waker hacked game save'
    # See also: https://github.com/FIX94?utf8=%E2%9C%93&tab=repositories&q=gamecube
    rm -rf WWHack*
    curl -fL https://github.com/FIX94/ww-hack-gc/releases/download/${my_wwhack_ver}/WWHack-GC-${my_wwhack_ver}.zip -o WWHack-GC-${my_wwhack_ver}.zip
    mkdir -p WWHack-GC-${my_wwhack_ver}
    pushd WWHack-GC-${my_wwhack_ver} >/dev/null
        unzip -q ../WWHack-GC-${my_wwhack_ver}.zip
    popd >/dev/null
    # Copy the hacked save file
    rsync -avP WWHack-GC-${my_wwhack_ver}/gzle.gci ./"${my_sd_name}"/MCBACKUP/

    # Copy Swiss over as the bootable launcher
    rsync -avP swiss_${my_swiss_ver}/GCI/boot.gci ./"${my_sd_name}"/MCBACKUP/

    echo ''
    echo 'Download and extract Game Boy Interface'
    rm -rf gbi*
    curl -fL https://files.extremscorner.org/gamecube/apps/gbi/latest -o gbi-${my_gbi_ver}.zip
    mkdir -p gbi-${my_gbi_ver}
    pushd gbi-${my_gbi_ver} >/dev/null
        unzip -q ../gbi-${my_gbi_ver}.zip
    popd >/dev/null

    # Copy Game Boy Interface as bootable from swiss
    rsync -avP gbi-${my_gbi_ver}/gbi.dol ./"${my_sd_name}"/
    echo -- '--zoom=2.875' > ./"${my_sd_name}"/gbi.cli

    # OR you could boot directly to any dol software that has been converted to gci format
    rsync -avP gbi-${my_gbi_ver}/MCBACKUP/gbi.gci ./"${my_sd_name}"/MCBACKUP/
popd >/dev/null
sleep 1

echo ''
diskutil list
echo "!!! BE CAREFUL !!!"
read -p "Which WHOLE DISK should be ERASED AND FORMATTED? (ex: /dev/disk8): " my_disk
diskutil unmountDisk $my_disk
#sudo newfs_msdos -F 16 -v "${my_sd_name}" $my_disk
sudo newfs_msdos -F 32 -v "${my_sd_name}" $my_disk
diskutil mount $my_disk

rsync -av ~/Downloads/"${my_sd_name}"/ /Volumes/"${my_sd_name}"/
diskutil unmount $my_disk

echo ''
echo 'Done!'